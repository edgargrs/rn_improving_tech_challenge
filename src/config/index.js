import AsyncStorage from '@react-native-async-storage/async-storage';

export const DebugConfig = {
  useFixtures: false,
  yellowBox: false,
  useReactotron: __DEV__,
  useDevTools: __DEV__,
};

export const REDUX_PERSIST = {
  key: 'rn_improving_tech_challenge',
  version: 1.0,
  storage: AsyncStorage,
  blacklist: ['splash'],
  whitelist: ['auth'],
};

export const baseURL = __DEV__
  ? 'https://kitsu.io/api/edge/'
  : 'https://kitsu.io/api/edge/';
export const reactotronURL = '192.168.0.100';
