import {configureStore as configStore} from '@reduxjs/toolkit';
import {persistReducer, persistStore} from 'redux-persist';
import {REDUX_PERSIST, DebugConfig} from '@config';
import Reactotron from '@config/ReactotronConfig';
import thunk from 'redux-thunk';
import reducers from 'reducers';

const persistedReducer = persistReducer(REDUX_PERSIST, reducers);
const middlewares = [thunk];

const store = configStore({
  reducer: persistedReducer,
  devTools: DebugConfig.useDevTools,
  enhancers: DebugConfig.useReactotron ? [Reactotron.createEnhancer()] : [],
  middleware: middlewares,
});

const persistor = persistStore(store);

export {store, persistor};
