import React from 'react';
import {View} from 'react-native';
import {Badge} from 'react-native-paper';
import {ms} from 'react-native-size-matters';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {DefaultTheme} from 'react-native-paper';

import {Styles} from './IconBadgeStyles';

const {colors} = DefaultTheme;

export const IconBadge = ({
  name = 'home',
  size = 20,
  color = colors.primary,
  badgeNumer = 0,
}) => {
  return (
    <View style={Styles.badgeIconView}>
      <Badge style={Styles.badge} visible={badgeNumer !== 0}>
        {badgeNumer > 9 ? '9+' : badgeNumer}
      </Badge>
      <Icon
        name={name}
        size={ms(size)}
        color={color}
        style={{width: size, height: size}}
      />
    </View>
  );
};
