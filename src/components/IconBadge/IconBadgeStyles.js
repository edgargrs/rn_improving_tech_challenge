import {ScaledSheet} from 'react-native-size-matters';

export const Styles = ScaledSheet.create({
  badgeIconView: {
    position: 'relative',
    padding: 5,
  },
  badge: {
    position: 'absolute',
    zIndex: 10,
    top: '-1@ms',
    right: '-8@ms',
    padding: 1,
  },
});
