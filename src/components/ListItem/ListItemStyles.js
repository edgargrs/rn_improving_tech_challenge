import {ScaledSheet} from 'react-native-size-matters';

export const Styles = ScaledSheet.create({
  image: {
    width: '100@ms',
  },
  row: {
    flexDirection: 'row',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    fontWeight: 'bold',
    color: 'white',
    fontSize: 32,
  },
  underlayLeft: {
    backgroundColor: 'tomato',
    justifyContent: 'flex-end',
    padding: '10@ms',
  },
});
