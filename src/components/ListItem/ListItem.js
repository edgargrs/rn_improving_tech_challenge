import React from 'react';
import {List} from 'react-native-paper';
import {ms} from 'react-native-size-matters';
import {Image, View, TouchableOpacity} from 'react-native';
import SwipeableItem, {
  useSwipeableItemParams,
} from 'react-native-swipeable-item';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import {Styles} from './ListItemStyles';

const UnderlayLeft = ({removeItems, id}) => {
  const {close} = useSwipeableItemParams();

  const handleRemove = () => {
    close();
    removeItems([id]);
  };

  return (
    <View style={[Styles.row, Styles.underlayLeft]}>
      <TouchableOpacity onPress={handleRemove}>
        <Icon name={'delete'} size={ms(30)} color={'white'} />
      </TouchableOpacity>
    </View>
  );
};

export const ListItem = ({item, removeItems}) => {
  const {
    attributes: {
      description,
      titles: {en},
      posterImage: {tiny: uri},
    },
  } = item;
  return (
    <SwipeableItem
      key={item.id}
      item={item}
      renderUnderlayLeft={() => (
        <UnderlayLeft removeItems={removeItems} id={item.id} />
      )}
      snapPointsLeft={[120]}>
      <List.Item
        title={en}
        description={description}
        titleNumberOfLines={1}
        descriptionNumberOfLines={3}
        left={() => (
          <Image source={{uri}} style={Styles.image} resizeMode={'cover'} />
        )}
      />
    </SwipeableItem>
  );
};
