import React from 'react';
import {ActivityIndicator} from 'react-native';
import {DefaultTheme} from 'react-native-paper';

import {Styles} from './LoadingStyles';

const {colors} = DefaultTheme;

export const Loading = () => {
  return (
    <ActivityIndicator
      color={colors.primary}
      size="large"
      style={Styles.loading}
    />
  );
};
