import React from 'react';
import {Card as PaperCard, Title, Paragraph} from 'react-native-paper';

import {Styles} from './CardStyles';

export const Card = ({item, height = 420, width = 300}) => {
  const {
    attributes: {
      description,
      titles: {en},
      posterImage: {small: uri},
    },
  } = item;
  return (
    <PaperCard style={[{height, width}]}>
      <PaperCard.Cover
        source={{uri}}
        resizeMode={'cover'}
        style={Styles.cover}
      />
      <PaperCard.Content>
        <Title numberOfLines={2}>{en}</Title>
        <Paragraph numberOfLines={4}>{description}</Paragraph>
      </PaperCard.Content>
    </PaperCard>
  );
};
