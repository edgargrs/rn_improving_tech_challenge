import {STRINGS} from './strings';
import {ROUTES} from './routes';
import {REGEX} from './regex';
import {RULES} from './rules';

export {STRINGS, ROUTES, REGEX, RULES};
