import {STRINGS, REGEX} from '@constants';
const {EMAIL, PASSWORD, INVALID_EMAIL, IS_REQUIRED, NAME} = STRINGS;

const {EMAIL_REGEX} = REGEX;

const EMAIL_RULES = {
  pattern: {
    value: EMAIL_REGEX,
    message: INVALID_EMAIL,
  },
  required: {
    value: true,
    message: EMAIL + IS_REQUIRED,
  },
};

const PASSWORD_RULES = {
  required: {value: true, message: PASSWORD + IS_REQUIRED},
};

const NAME_RULES = {
  required: {value: true, message: NAME + IS_REQUIRED},
};

export const RULES = {
  PASSWORD_RULES,
  EMAIL_RULES,
  NAME_RULES,
};
