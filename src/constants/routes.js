export const ROUTES = {
  SPLASH: 'Splash',
  WELCOME: 'Welcome',
  LOGIN: 'Login',
  HOME: 'Home',
  MY_PHOTOS: 'My Photos',
  PROFILE: 'Profile',
  PROFILE_NAVIGATOR: 'Profile navigator',
  EDIT_PROFILE: 'Edit Profile',
};
