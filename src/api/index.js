import axios from 'axios';
import {baseURL} from '@config';

const api = axios.create({
  baseURL,
  timeout: 15000,
});

const createInterceptor = () => {
  api.interceptors.response.use(
    response => response,
    error => {
      let errorResponse = error.response;
      switch (errorResponse.status) {
        case 401:
          break;
        case 406:
          break;
        case 415:
          break;
        case 500:
          break;
        default:
          break;
      }
    },
  );
};

createInterceptor();

const getAnimes = (page = 0, limit = 10) =>
  api.get(`anime?page[limit]=${limit}&page[offset]=${page}`);

export {getAnimes};
