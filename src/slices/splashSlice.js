import {createSlice} from '@reduxjs/toolkit';

const initialState = {
  isAppLoaded: false,
};

export const splashSlice = createSlice({
  name: 'splash',
  initialState,
  reducers: {
    setAppLoaded: (state, action) => {
      state.isAppLoaded = action.payload;
    },
  },
});

export const {setAppLoaded} = splashSlice.actions;

export default splashSlice.reducer;
