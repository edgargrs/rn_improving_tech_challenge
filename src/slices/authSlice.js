import {createSlice} from '@reduxjs/toolkit';

const initialState = {
  token: null,
  user: {
    user_name: '',
    email: '',
    password: '',
    avatar: null,
    location: null,
  },
};

export const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    setUser: (state, action) => {
      const token = [...Array(30)]
        .map(() => Math.random().toString(36)[2])
        .join('');
      return {...state, token, user: {...state.user, ...action.payload}};
    },
    logout: () => {
      return initialState;
    },
  },
});

export const {setUser, logout} = authSlice.actions;

export default authSlice.reducer;
