import {ScaledSheet} from 'react-native-size-matters';
import {Dimensions} from 'react-native';

const {width} = Dimensions.get('window');

export const Styles = ScaledSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  carouselStyle: {
    width,
    justifyContent: 'center',
    marginBottom: '40@ms',
  },
  card: {
    width: '80%',
  },
});
