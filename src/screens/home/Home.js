import React, {useCallback} from 'react';
import {ms} from 'react-native-size-matters';
import {View, Dimensions} from 'react-native';
import Carousel from 'react-native-reanimated-carousel';
import {Appbar} from 'react-native-paper';
import {useList} from '@hooks/useList';
import {Card} from '@components/Card/Card';
import {Loading} from '@components/Loading/Loading';

import {ROUTES} from '@constants';
import {Styles} from './HomeStyles';

const {HOME} = ROUTES;

const {width} = Dimensions.get('screen');

export const Home = () => {
  const {listLoading, list} = useList(5);

  const renderItem = useCallback(
    ({item}) => (
      <Card item={item} height={ms(450)} width={width - width * 0.2} />
    ),
    [],
  );

  return (
    <>
      <Appbar.Header>
        <Appbar.Content title={HOME} />
      </Appbar.Header>
      <View style={Styles.container}>
        {listLoading ? (
          <Loading />
        ) : (
          <Carousel
            style={Styles.carouselStyle}
            autoPlay={false}
            height={ms(480)}
            width={ms(300)}
            data={list}
            loop
            mode="parallax"
            modeConfig={{
              parallaxScrollingScale: 0.9,
              parallaxScrollingOffset: 30,
            }}
            renderItem={renderItem}
          />
        )}
      </View>
    </>
  );
};
