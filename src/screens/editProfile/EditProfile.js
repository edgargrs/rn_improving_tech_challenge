import React, {useState} from 'react';
import {View, TouchableOpacity, Image} from 'react-native';
import {ms} from 'react-native-size-matters';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {useForm, Controller} from 'react-hook-form';
import {useDispatch, useSelector} from 'react-redux';
import {Appbar, TextInput, Title, HelperText} from 'react-native-paper';
import {launchImageLibrary} from 'react-native-image-picker';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import {getuser} from '@selectors/authSelectors.js';
import {setUser} from '@slices/authSlice.js';

import {ROUTES, STRINGS, RULES} from '@constants';
import {Styles} from './EditProfileStyles';

const {EDIT_PROFILE} = ROUTES;
const {EMAIL_RULES, NAME_RULES} = RULES;
const {EMAIL, PASSWORD, NAME, UPDATE_PROFILE_PICTURE} = STRINGS;

const IMAGE_OPTIONS = {
  mediaType: 'photo',
  maxWidth: 100,
  maxHeight: 100,
  quality: 0.5,
  includeBase64: true,
  saveToPhotos: true,
};

export const EditProfile = ({navigation}) => {
  const user = useSelector(getuser);
  const [secureTextEntry, setSecureTextEntry] = useState(true);
  const [image, setImage] = useState(user?.avatar);
  const dispatch = useDispatch();
  const {
    control,
    handleSubmit,
    formState: {errors, isValid},
  } = useForm({mode: 'onBlur'});

  const toggleSecureTextEntry = () => setSecureTextEntry(!secureTextEntry);

  const navigateBack = () => navigation.goBack();

  const handleOnSubmit = input => {
    if (isValid) {
      Object.keys(input).forEach(key => {
        if (input[key] === '' || input[key] == null) {
          delete input[key];
        }
      });
      let data = input;
      if (image && image !== user?.avatar) {
        data = {...input, avatar: image};
      }
      dispatch(setUser(data));
      navigateBack();
    }
  };

  const selectProfileImage = async () => {
    try {
      const result = await launchImageLibrary(IMAGE_OPTIONS);
      if (result?.assets) {
        const {type, base64} = result.assets[0];
        setImage(`data:${type};base64,${base64}`);
      }
    } catch (error) {
      console.log('Problem getting image');
    }
  };

  return (
    <>
      <Appbar.Header>
        <Appbar.BackAction onPress={navigateBack} />
        <Appbar.Content title={EDIT_PROFILE} />
        <Appbar.Action
          icon="account-check"
          onPress={handleSubmit(handleOnSubmit)}
        />
      </Appbar.Header>
      <KeyboardAwareScrollView
        contentContainerStyle={Styles.container}
        scrollEnabled>
        <View style={Styles.innerContainer}>
          <View style={Styles.imageSection}>
            <Title>{UPDATE_PROFILE_PICTURE}</Title>
            <TouchableOpacity
              style={Styles.imageContainer}
              activeOpacity={0.5}
              onPress={selectProfileImage}>
              {image ? (
                <Image
                  style={Styles.image}
                  resizeMode="cover"
                  source={{uri: image}}
                />
              ) : (
                <Icon name={'camera'} size={ms(30)} color={'black'} />
              )}
            </TouchableOpacity>
          </View>

          <Controller
            name={'user_name'}
            defaultValue={user?.user_name}
            control={control}
            render={({field: {value, onBlur, onChange}}) => (
              <TextInput
                onChangeText={onChange}
                label={NAME}
                value={value}
                style={Styles.input}
                onBlur={onBlur}
              />
            )}
            rules={NAME_RULES}
          />
          <HelperText type="error" style={Styles.errorText}>
            {errors?.user_name?.message}
          </HelperText>

          <Controller
            name={'email'}
            defaultValue={user?.email}
            control={control}
            render={({field: {value, onBlur, onChange}}) => (
              <TextInput
                label={EMAIL}
                value={value}
                onChangeText={onChange}
                control={control}
                onBlur={onBlur}
              />
            )}
            rules={EMAIL_RULES}
          />
          <HelperText type="error" style={Styles.errorText}>
            {errors?.email?.message}
          </HelperText>

          <Controller
            name={'password'}
            defaultValue={''}
            render={({field: {value, onBlur, onChange}}) => (
              <TextInput
                label={PASSWORD}
                value={value}
                onChangeText={onChange}
                secureTextEntry={secureTextEntry}
                right={
                  value.length > 0 && (
                    <TextInput.Icon
                      name="eye"
                      onPress={toggleSecureTextEntry}
                      forceTextInputFocus={false}
                    />
                  )
                }
                onBlur={onBlur}
              />
            )}
            control={control}
          />
          <HelperText type="error" style={Styles.errorText}>
            {errors?.password?.message}
          </HelperText>
        </View>
      </KeyboardAwareScrollView>
    </>
  );
};
