import {ScaledSheet} from 'react-native-size-matters';

export const Styles = ScaledSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
  },
});
