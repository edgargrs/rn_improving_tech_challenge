import React, {useCallback, useState} from 'react';
import {View, TouchableOpacity, RefreshControl, Platform} from 'react-native';
import {createNativeWrapper} from 'react-native-gesture-handler';
import {Appbar, DefaultTheme} from 'react-native-paper';
import DraggableFlatList, {
  ScaleDecorator,
} from 'react-native-draggable-flatlist';

const AndroidRefreshControl = createNativeWrapper(RefreshControl, {
  disallowInterruption: true,
  shouldCancelWhenOutside: false,
});

import {useList} from '@hooks/useList';
import {ListItem} from '@components/ListItem/ListItem';
import {Loading} from '@components/Loading/Loading';

import {ROUTES} from '@constants';

import {Styles} from './MyPhotosSyles';

const {MY_PHOTOS} = ROUTES;
const {colors} = DefaultTheme;

export const MyPhotos = () => {
  const [refreshControlEnabled, setRefreshControlEnabled] = useState(true);
  const {
    listLoading,
    list,
    updateList,
    requestList,
    listRefreshing,
    removeItems,
  } = useList(10);

  const renderItem = ({item, drag, isActive}) => {
    return (
      <ScaleDecorator>
        <TouchableOpacity
          onLongPress={drag}
          disabled={isActive}
          activeOpacity={0.5}>
          <ListItem item={item} removeItems={removeItems} />
        </TouchableOpacity>
      </ScaleDecorator>
    );
  };

  const keyExtractor = useCallback((item, index) => `${index}_${item?.id}`, []);

  const handleOnRefresh = () => requestList(true);

  return (
    <>
      <Appbar.Header>
        <Appbar.Content title={MY_PHOTOS} />
      </Appbar.Header>
      <View style={Styles.container}>
        {listLoading ? (
          <Loading />
        ) : (
          <DraggableFlatList
            data={list}
            onDragBegin={() => {
              setRefreshControlEnabled(false);
            }}
            onDragEnd={({data}) => {
              setRefreshControlEnabled(true);
              updateList(data);
            }}
            renderItem={renderItem}
            activationDistance={20}
            keyExtractor={keyExtractor}
            refreshControl={
              listRefreshing !== null ? (
                Platform.OS === 'android' ? (
                  <AndroidRefreshControl
                    refreshing={listRefreshing}
                    onRefresh={handleOnRefresh}
                    tintColor={colors.primary}
                    enabled={refreshControlEnabled}
                  />
                ) : (
                  <RefreshControl
                    refreshing={listRefreshing}
                    onRefresh={handleOnRefresh}
                    tintColor={colors.primary}
                  />
                )
              ) : null
            }
          />
        )}
      </View>
    </>
  );
};
