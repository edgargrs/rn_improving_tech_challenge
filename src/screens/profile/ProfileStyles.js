import {ms, ScaledSheet} from 'react-native-size-matters';

export const Styles = ScaledSheet.create({
  container: {
    padding: '10@ms',
    justifyContent: 'space-between',
  },
  innerContainer: {
    flex: 1,
  },
  imageSection: {
    alignItems: 'center',
  },
  imageContainer: {
    width: ms(120),
    aspectRatio: 1,
    backgroundColor: 'white',
    borderRadius: '100@ms',
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    width: ms(120),
    aspectRatio: 1,
    borderRadius: '100@ms',
  },
  input: {
    marginTop: '15@ms',
  },
  logoutButton: {
    marginTop: '100@ms',
  },
});
