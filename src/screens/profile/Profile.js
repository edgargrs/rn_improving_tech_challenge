import React, {useState} from 'react';
import {ScrollView, View, TouchableOpacity, Image} from 'react-native';
import {ms} from 'react-native-size-matters';
import {useDispatch, useSelector} from 'react-redux';
import {
  Appbar,
  Button,
  TextInput,
  Title,
  Paragraph,
  Dialog,
  Portal,
  Provider,
} from 'react-native-paper';
import {getBrand, getSystemVersion, getModel} from 'react-native-device-info';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import {logout} from '@slices/authSlice';
import {getuser} from '@selectors/authSelectors.js';

import {ROUTES, STRINGS} from '@constants';
import {Styles} from './ProfileStyles';

const {PROFILE, EDIT_PROFILE} = ROUTES;
const {
  EMAIL,
  PASSWORD,
  NAME,
  DEVICE_INFO,
  PROFILE_PICTURE,
  LOGOUT,
  LOGOUT_MESSAGE,
  OK,
  CANCEL,
} = STRINGS;

const deviceInfo =
  getBrand() + ' ' + getModel() + ', OS: ' + getSystemVersion();

export const Profile = ({navigation}) => {
  const user = useSelector(getuser);
  const dispatch = useDispatch();
  const navigateToEditProfile = () => navigation.navigate(EDIT_PROFILE);

  const [dilogVisible, setVisible] = useState(false);

  const showDialog = () => setVisible(true);

  const hideDialog = () => setVisible(false);

  const handleLogout = () => {
    hideDialog();
    dispatch(logout());
  };

  return (
    <>
      <Appbar.Header>
        <Appbar.Content title={PROFILE} />
        <Appbar.Action icon="account-edit" onPress={navigateToEditProfile} />
      </Appbar.Header>
      <Provider>
        <ScrollView contentContainerStyle={Styles.container} scrollEnabled>
          <View style={Styles.innerContainer}>
            <View style={Styles.imageSection}>
              <Title>{PROFILE_PICTURE}</Title>
              <TouchableOpacity
                style={Styles.imageContainer}
                activeOpacity={0.5}
                onPress={navigateToEditProfile}>
                {user?.avatar ? (
                  <Image
                    style={Styles.image}
                    resizeMode="cover"
                    source={{uri: user?.avatar}}
                  />
                ) : (
                  <Icon name={'camera'} size={ms(30)} color={'black'} />
                )}
              </TouchableOpacity>
            </View>
            <TextInput
              editable={false}
              label={NAME}
              value={user?.user_name ?? ' '}
              style={Styles.input}
            />
            <TextInput
              editable={false}
              label={EMAIL}
              value={user?.email}
              style={Styles.input}
            />
            <TextInput
              editable={false}
              label={DEVICE_INFO}
              value={deviceInfo}
              style={Styles.input}
            />
            <TextInput
              editable={false}
              label={PASSWORD}
              value={user?.password}
              style={Styles.input}
              secureTextEntry
            />
            <Button
              mode="contained"
              onPress={showDialog}
              style={Styles.logoutButton}>
              {LOGOUT}
            </Button>
          </View>
        </ScrollView>
      </Provider>
      <Portal>
        <Dialog visible={dilogVisible} onDismiss={hideDialog}>
          <Dialog.Title>{LOGOUT}</Dialog.Title>
          <Dialog.Content>
            <Paragraph>{LOGOUT_MESSAGE}</Paragraph>
          </Dialog.Content>
          <Dialog.Actions>
            <Button onPress={hideDialog}>{CANCEL}</Button>
            <Button onPress={handleLogout}>{OK}</Button>
          </Dialog.Actions>
        </Dialog>
      </Portal>
    </>
  );
};
