import React, {useEffect} from 'react';
import RNBootSplash from 'react-native-bootsplash';
import {View} from 'react-native';
import {useDispatch} from 'react-redux';
import {ActivityIndicator} from 'react-native-paper';

import {setAppLoaded} from '@slices/splashSlice';

import {Styles} from './SplashStyles';

export const Splash = () => {
  const dispatch = useDispatch();
  useEffect(() => {
    RNBootSplash.hide({fade: true});
    setTimeout(() => {
      dispatch(setAppLoaded(true));
    }, 1000);
  }, [dispatch]);
  return (
    <View style={Styles.container}>
      <ActivityIndicator animating size={'large'} />
    </View>
  );
};
