import React from 'react';
import {View} from 'react-native';
import {ms} from 'react-native-size-matters';
import {Avatar, Button, Title} from 'react-native-paper';
import {images} from '@assets/images/images';
import {STRINGS, ROUTES} from '@constants';

import {Styles} from './WelcomeStyles';

const {logo} = images;
const {CONTINUE, WELCOME} = STRINGS;
const {LOGIN} = ROUTES;

export const Welcome = ({navigation}) => {
  const navigateToLogin = () => navigation.navigate(LOGIN);
  return (
    <View style={Styles.container}>
      <Avatar.Image size={ms(150)} source={logo} />
      <View style={Styles.alignCenter}>
        <Title style={Styles.text}>{WELCOME}</Title>
        <Button mode="contained" onPress={navigateToLogin}>
          {CONTINUE}
        </Button>
      </View>
    </View>
  );
};
