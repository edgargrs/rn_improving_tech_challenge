import {ScaledSheet} from 'react-native-size-matters';
import {DefaultTheme} from 'react-native-paper';

const {colors} = DefaultTheme;

export const Styles = ScaledSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-evenly',
    alignItems: 'center',
    backgroundColor: colors.background,
  },
  alignCenter: {
    alignItems: 'center',
  },
  text: {
    color: colors.text,
    paddingBottom: '30@ms',
  },
});
