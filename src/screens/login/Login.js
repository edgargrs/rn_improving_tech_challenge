import React, {useState} from 'react';
import {View} from 'react-native';
import {ms} from 'react-native-size-matters';
import {useForm, Controller} from 'react-hook-form';
import {Avatar, Button, TextInput, HelperText, Title} from 'react-native-paper';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {STRINGS, RULES} from '@constants';
import {useAuthHook} from '@hooks/useAuthHook';

import {images} from '@assets/images/images';
import {Styles} from './LoginStyles';

const {logo} = images;

const {LOGIN, EMAIL, PASSWORD} = STRINGS;
const {EMAIL_RULES, PASSWORD_RULES} = RULES;

export const Login = () => {
  const [secureTextEntry, setSecureTextEntry] = useState(true);
  const toggleSecureTextEntry = () => setSecureTextEntry(!secureTextEntry);
  const {handleLogin, isLoading} = useAuthHook();

  const {
    control,
    handleSubmit,
    formState: {errors},
  } = useForm({mode: 'onBlur'});

  return (
    <KeyboardAwareScrollView contentContainerStyle={Styles.container}>
      <Avatar.Image size={ms(150)} source={logo} style={Styles.logo} />
      <View style={Styles.form}>
        <Title>{LOGIN}</Title>
        <Controller
          name={'email'}
          defaultValue={''}
          control={control}
          render={({field: {value, onBlur, onChange}}) => (
            <TextInput
              label={EMAIL}
              value={value}
              onChangeText={onChange}
              editable={!isLoading}
              control={control}
              onBlur={onBlur}
            />
          )}
          rules={EMAIL_RULES}
        />
        <HelperText type="error" style={Styles.errorText}>
          {errors?.email?.message}
        </HelperText>
        <Controller
          name={'password'}
          defaultValue={''}
          render={({field: {value, onBlur, onChange}}) => (
            <TextInput
              label={PASSWORD}
              value={value}
              onChangeText={onChange}
              secureTextEntry={secureTextEntry}
              right={
                value.length > 0 && (
                  <TextInput.Icon
                    name="eye"
                    onPress={toggleSecureTextEntry}
                    forceTextInputFocus={false}
                  />
                )
              }
              editable={!isLoading}
              onBlur={onBlur}
            />
          )}
          control={control}
          rules={PASSWORD_RULES}
        />
        <HelperText type="error" style={Styles.errorText}>
          {errors?.password?.message}
        </HelperText>
        <Button
          mode="contained"
          onPress={handleSubmit(handleLogin)}
          loading={isLoading}>
          {LOGIN}
        </Button>
      </View>
    </KeyboardAwareScrollView>
  );
};
