import {ScaledSheet} from 'react-native-size-matters';
import {DefaultTheme} from 'react-native-paper';
import {Platform} from 'react-native';

const {colors} = DefaultTheme;

export const Styles = ScaledSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: '20@ms',
    alignContent: 'center',
    justifyContent: 'space-evenly',
    backgroundColor: colors.background,
  },
  logo: {
    alignSelf: 'center',
  },
  form: {},
  errorText: {
    paddingBottom: '10@ms',
  },
});
