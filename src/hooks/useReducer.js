import {useReducer as useReducerNative} from 'react';
import useIsMounted from '@hooks/useIsMounted';

export const useReducer = initialState => {
  const isMounted = useIsMounted();
  const [store, dispatch] = useReducerNative(
    (state, newState) => ({...state, ...newState}),
    initialState,
  );

  const setState = data => {
    if (isMounted()) {
      dispatch({...data});
    }
  };

  return [store, setState];
};
