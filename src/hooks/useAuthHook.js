import {useDispatch} from 'react-redux';
import {useReducer} from '@hooks/useReducer';
import {setUser} from '@slices/authSlice';

const initialState = {
  isLoading: false,
};

export const useAuthHook = () => {
  const dispatch = useDispatch();
  const [{isLoading}, setState] = useReducer(initialState);

  const handleLogin = input => {
    if (isLoading) {
      return;
    }
    setState({isLoading: true});
    setTimeout(() => {
      // Hit endpoint
      dispatch(setUser(input));
      setState({isLoading: false});
    }, 2000);
  };

  return {
    handleLogin,
    isLoading,
  };
};
