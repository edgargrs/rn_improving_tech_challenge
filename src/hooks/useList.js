import {useEffect, useCallback} from 'react';
import {useFocusEffect} from '@react-navigation/native';
import {useReducer} from '@hooks/useReducer';
import {getAnimes} from '@api';

const initialState = {
  listRefreshing: false,
  listFirstLoad: true,
  listLoading: true,
  listError: null,
  listPage: 0,
  listEnd: false,
  list: [],
};

export const useList = (limit = 10, focusRefresh = false) => {
  const [
    {
      listRefreshing,
      listFirstLoad,
      listLoading,
      listError,
      listPage,
      listEnd,
      list,
    },
    setState,
  ] = useReducer(initialState);

  useEffect(() => {
    if (!focusRefresh) {
      requestList();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useFocusEffect(
    useCallback(() => {
      if (focusRefresh) {
        requestList(true);
      }
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []),
  );

  const requestList = async (isRefreshing = false) => {
    if (listEnd && !isRefreshing) {
      return;
    }

    if (isRefreshing) {
      setState({
        listRefreshing: true,
        listFirstLoad: true,
        listLoading: true,
        listPage: 0,
        listEnd: false,
        list: [],
      });
    } else {
      setState({listLoading: true});
    }
    try {
      const {data} = await getAnimes(isRefreshing ? 0 : listPage, limit);
      const items = data.data;
      const newItems = isRefreshing ? items : [...list, ...items];
      setState({
        listRefreshing: false,
        listPage: listRefreshing ? 0 : listPage + limit,
        listFirstLoad: false,
        listLoading: false,
        listError: null,
        listEnd: items.length === 0,
        list: newItems,
      });
    } catch (err) {
      setState({
        listLoading: false,
        listError: err,
      });
      console.log('Error getting photos', err);
    }
  };

  const removeItems = async items => {
    const filterdList = list.filter(item => !items.includes(item.id));
    setState({list: filterdList});
  };

  const updateList = newList => setState({list: newList});

  return {
    listRefreshing,
    listFirstLoad,
    listLoading,
    requestList,
    removeItems,
    updateList,
    listError,
    listEnd,
    list,
  };
};
