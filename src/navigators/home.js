import React from 'react';
import {Platform} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {DefaultTheme} from 'react-native-paper';
import {ms} from 'react-native-size-matters';
import {IconBadge} from '@components/IconBadge/IconBadge.js';

import {Home} from '@screens/home/Home';
import {MyPhotos} from '@screens/myPhotos/MyPhotos';
import {ProfileNavigator} from '@navigators/profile';

import {ROUTES} from '@constants';

const {HOME, MY_PHOTOS, PROFILE, PROFILE_NAVIGATOR} = ROUTES;

const {colors} = DefaultTheme;

const TabsAndroid = createMaterialBottomTabNavigator();
const TabsIOS = createBottomTabNavigator();

const TAB_PROPS = {
  barStyle: {backgroundColor: colors.primary},
  screenOptions: ({route}) => ({
    headerShown: false,
    tabBarActiveTintColor: colors.primary,
    tabBarStyle: {
      borderTopColor: colors.primary,
      borderTopWidth: 0,
      elevation: 0,
    },
    tabBarLabelStyle: {
      fontSize: ms(15),
    },
    tabBarIcon: ({color}) => {
      let iconName = '';
      let badgeNumber = 0;
      switch (route.name) {
        case HOME:
          iconName = 'home';
          break;
        case MY_PHOTOS:
          iconName = 'image';
          break;
        case PROFILE_NAVIGATOR:
          iconName = 'face-man';
          badgeNumber = 3;
          break;
      }
      return (
        <IconBadge
          name={iconName}
          size={ms(20)}
          color={color}
          badgeNumer={badgeNumber}
        />
      );
    },
  }),
};

export const HomeNavigator = () => {
  // const {top: paddingTop} = useSafeAreaInsets();
  const Tabs = Platform.OS === 'ios' ? TabsIOS : TabsAndroid;
  return (
    <>
      <Tabs.Navigator
        {...TAB_PROPS}
        sceneContainerStyle={{
          backgroundColor: colors.background,
          // paddingTop,
        }}>
        <Tabs.Screen
          name={HOME}
          options={{
            title: HOME,
          }}
          component={Home}
        />
        <Tabs.Screen
          name={MY_PHOTOS}
          options={{
            title: MY_PHOTOS,
          }}
          component={MyPhotos}
        />
        <Tabs.Screen
          name={PROFILE_NAVIGATOR}
          options={{
            title: PROFILE,
          }}
          component={ProfileNavigator}
        />
      </Tabs.Navigator>
    </>
  );
};
