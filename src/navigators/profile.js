import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import {Profile} from '@screens/profile/Profile';
import {EditProfile} from '@screens/editProfile/EditProfile';

import {ROUTES} from '@constants';

const {PROFILE, EDIT_PROFILE} = ROUTES;

const Stack = createNativeStackNavigator();

export const ProfileNavigator = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen name={PROFILE} component={Profile} />
      <Stack.Screen name={EDIT_PROFILE} component={EditProfile} />
    </Stack.Navigator>
  );
};
