import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {StatusBar} from 'react-native';
import {useSelector} from 'react-redux';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {DefaultTheme} from 'react-native-paper';

import {HomeNavigator} from '@navigators/home';
import {WelcomeNavigator} from '@navigators/welcome';

import {Splash} from '@screens/splash/Splash';

import {getisAppLoaded} from '@selectors/splashSelectors';
import {isLoggedIn} from '@selectors/authSelectors';

const {colors} = DefaultTheme;

export const Navigation = () => {
  const appIsLoaded = useSelector(getisAppLoaded);
  const isAuthenticated = useSelector(isLoggedIn);

  if (!appIsLoaded) {
    return <Splash />;
  }

  return (
    <NavigationContainer>
      <SafeAreaProvider>
        <StatusBar backgroundColor={colors.primary} animated />
        {isAuthenticated ? <HomeNavigator /> : <WelcomeNavigator />}
      </SafeAreaProvider>
    </NavigationContainer>
  );
};
