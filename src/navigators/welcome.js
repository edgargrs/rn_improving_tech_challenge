import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {useSafeAreaInsets} from 'react-native-safe-area-context';

import {Welcome} from '@screens/welcome/Welcome';
import {Login} from '@screens/login/Login';

import {ROUTES} from '@constants';

const {WELCOME, LOGIN} = ROUTES;

const Stack = createNativeStackNavigator();

export const WelcomeNavigator = () => {
  const {top: paddingTop} = useSafeAreaInsets();
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
        contentStyle: {
          paddingTop,
        },
      }}>
      <Stack.Screen name={WELCOME} component={Welcome} />
      <Stack.Screen name={LOGIN} component={Login} />
    </Stack.Navigator>
  );
};
