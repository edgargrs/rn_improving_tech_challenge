import {combineReducers} from 'redux';

import SplashReducer from '@slices/splashSlice';
import AuthSlice from '@slices/authSlice';

export default combineReducers({
  splash: SplashReducer,
  auth: AuthSlice,
});
