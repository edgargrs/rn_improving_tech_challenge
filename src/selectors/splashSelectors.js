import {createSelector} from 'reselect';

export const getSplashState = state => state.splash;

export const getisAppLoaded = createSelector(
  [getSplashState],
  splash => splash.isAppLoaded,
);
