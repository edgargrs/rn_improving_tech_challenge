import {createSelector} from 'reselect';

export const getAuthState = state => state.auth;

export const isLoggedIn = createSelector(
  [getAuthState],
  auth => auth.token !== null,
);

export const getuser = createSelector([getAuthState], auth => auth.user);
