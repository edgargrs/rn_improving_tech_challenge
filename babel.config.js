module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    [
      'module-resolver',
      {
        root: ['./src'],
        extensions: ['.jsx', '.js', '.json'],
        alias: {
          '@assets': './src/assets',
          '@components': './src/components',
          '@api': './src/api',
          '@hooks': './src/hooks',
          '@constants': './src/constants',
          '@navigators': './src/navigators',
          '@screens': './src/screens',
          '@config': './src/config',
          '@slices': './src/slices',
          '@selectors': './src/selectors',
        },
      },
    ],
    'react-native-reanimated/plugin',
  ],
};
