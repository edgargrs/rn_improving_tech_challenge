# rn_improving_tech_challenge

# Developed with React Native

This is a mobile application for a technical challenge from the company Improving.

## Project Versions

| Requeriment  | Version |
| ------------ | ------- |
| Node         | 16.15.1 |
| npm          | 8.11.0  |
| React        | 18.1.0  |
| React Native | 0.70.5  |

## Third Party Libraries Versions

| Dependency                | Version |
| ------------------------- | ------- |
| @react-native-async-storage/async-storage | ^1.17.11 |
| @react-native-community/netinfo | ^9.3.6 |
| @react-navigation/bottom-tabs | ^6.4.0 |
| @react-navigation/material-bottom-tabs | ^6.2.4 |
| @react-navigation/native | ^6.0.13 |
| @react-navigation/native-stack | ^6.9.1 |
| @reduxjs/toolkit | ^1.9.0 |
| axios | 0.27.2 |
| react | 18.1.0 |
| react-hook-form | ^7.39.4 |
| react-native | 0.70.5 |
| react-native-bootsplash | ^4.3.3 |
| react-native-device-info | ^10.3.0 |
| react-native-draggable-flatlist | ^3.1.2 |
| react-native-geolocation-service | ^5.3.1 |
| react-native-gesture-handler | ^2.8.0 |
| react-native-image-picker | ^4.10.1 |
| react-native-keyboard-aware-scroll-view | ^0.9.5 |
| react-native-modal | ^13.0.1 |
| react-native-paper | ^4.12.5 |
| react-native-reanimated | ^2.12.0 |
| react-native-reanimated-carousel | ^3.1.5 |
| react-native-safe-area-context | ^4.4.1 |
| react-native-screens | ^3.18.2 |
| react-native-size-matters | ^0.4.0 |
| react-native-swipeable-item | ^2.0.9 |
| react-native-vector-icons | ^9.2.0 |
| react-redux | ^8.0.5 |
| redux | ^4.2.0 |
| redux-batched-actions | ^0.5.0 |
| redux-persist | ^6.0.0 |
| redux-thunk | ^2.4.2 |

## Instructions to run the project

### Note:

- Need to have installed Android Studio and Xcode
- Xcode version used for this project: 14.1
- Android Studio version used Dolphin | 2021.3.1 Patch 1

#### Run instructions for iOS:

Requeriment: A Mac is required in order to build your app for iOs devices.

- Using a simulator in Xcode: Enter in the project folder && run the command `npx react-native run-ios`

#### Run instructions for Android:

- Using an emulator in Android Studio: Have an Android emulator running && Enter in the project folder && run the command `npx react-native run-android`

#### Note:

To fix issues (if there are errors after installing libraries / or doing yarn install) or during the first run, try these steps:

- Option 1: Run `npx pod-install ios` in the project folder and then rebuild and re-run the app.

- Option 2: Rebuild and restart the app.

- Option 3: Run the packager with `--reset-cache` flag. The full command is: `npx react-native start --reset-cache`

- Option 4: (Problems only with Android [app opens and closes immediately in emulator]) Run the command inside the project folder `cd android && ./gradlew clean`
