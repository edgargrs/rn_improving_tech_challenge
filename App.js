import React from 'react';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';
import {Provider as PaperProvider} from 'react-native-paper';
import {GestureHandlerRootView} from 'react-native-gesture-handler';
import {store, persistor} from '@config/configureStore';
import {Navigation} from '@navigators';

const GESTURE_HANDLER_ROOT_VIEW_STYLE = {flex: 1};

const App = () => {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <PaperProvider>
          <GestureHandlerRootView style={GESTURE_HANDLER_ROOT_VIEW_STYLE}>
            <Navigation />
          </GestureHandlerRootView>
        </PaperProvider>
      </PersistGate>
    </Provider>
  );
};

export default App;
